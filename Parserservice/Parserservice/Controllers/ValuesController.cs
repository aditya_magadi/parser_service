﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

using System.Xml.Linq;
using System.Runtime.Caching;



using java.io;
using edu.stanford.nlp.process;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.trees.tregex;
using edu.stanford.nlp.parser.lexparser;
using Console = System.Console;
using System.Text;
using System.Text.RegularExpressions;

namespace Parserservice.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            return "You need to pass values please pass like this --> parserservice.apphb.com/api/values/<string>";
        }

        // GET api/values/5
        public Result Get(string id)
        {
            controllerToDetermine cont = new controllerToDetermine();
            Result res = cont.Parse(id);
            return res;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
    public class controllerToDetermine
    {
        public Result Parse(string sentence)
        {
            TreebankLanguagePack tlp = new PennTreebankLanguagePack();
            var lp = LexicalParser.Instance;
            var sentenceToParse = sentence;
            var tokenizerFactory = tlp.getTokenizerFactory();
            var readSentenceToParse = new StringReader(sentenceToParse);
            var tokenizedWords = tokenizerFactory.getTokenizer(readSentenceToParse).tokenize();
            readSentenceToParse.close();
            var tokenTree = lp.apply(tokenizedWords);

            List<string> phraseList = new List<string>();

            foreach (Tree subtree in tokenTree)
            {

                if (subtree.label().value().Equals("NN") ||
                    subtree.label().value().Equals("NNS") ||
                    subtree.label().value().Equals("NNP") ||
                    subtree.label().value().Equals("NNPS") ||
                    subtree.label().value().Equals("VBG") ||
                    subtree.label().value().Equals("VBN")||
                    subtree.label().value().Equals("VB")||
                    subtree.label().value().Equals("JJ"))
                {
                    var yields = subtree.yield().toArray().Cast<CoreLabel>();
                    phraseList.AddRange(yields.Select(item => item.value()));


                }

            }

            var controllerId = GetControllerId(phraseList);
            if (controllerId == null)
                return Result.Failed(sentence);
            else
            {
                var screenNode = DomainInputCacheManager.Instance.GetDomainDoc().Element("root").Elements("screen")
                    .Where(elem => elem.Attribute("id") != null && elem.Attribute("id").Value.Equals(controllerId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (screenNode == null)
                    return Result.Failed(sentence);
                return new Result(screenNode, sentence, tokenTree.pennString());
            }

        }
        private string GetControllerId(List<string> phraseList)
        {
            var result = DomainInputCacheManager.Instance.GetDomainDetails().Search(phraseList);
            return result;
        }
    }
    public class Result
    {
        public string ControllerName { get; private set; }
        public string Action { get; private set; }
        public string Input { get; private set; }
        public bool Success { get; private set; }
        public IDictionary<string, string> Context { get; private set; }

        public Result(XElement screenNode, string input, string pennedInput)
        {
            this.ControllerName = screenNode.Attribute("controller").Value;
            this.Action = screenNode.Attribute("action").Value;
            this.Input = input;
            this.Success = true;
            var context = new Dictionary<string, string>();
            if (screenNode.Elements("inputs").Any())
            {
                foreach (var item in screenNode.Element("inputs").Elements("value"))
                {
                    string key = item.Attribute("name").Value;
                }
            }
        }

        private Result()
        {

        }

        public static Result Failed(string input)
        {

            return new Result() { Success = false, Input = input };

        }
    }
    public sealed class LexicalParser
    {
        private static LexicalizedParser instance = null;
        private static readonly object padlock = new object();

        LexicalParser()
        {
        }
        public static LexicalizedParser Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            var modelsDirectory = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_data/englishPCFG.caseless.ser.gz");
                            instance = LexicalizedParser.loadModel(modelsDirectory);
                        }
                    }
                }
                return instance;
            }


        }
    }
    public class MySubDomain
    {
        public IEnumerable<MySubDomain> Children { get; private set; }

        public string ScreenId { get; set; }
        public string Value { get; set; }
        public bool HasChildren { get; set; }

        public MySubDomain(XElement element)
        {
            var children = new List<MySubDomain>();
            this.Value = element.Attribute("value").Value;
            if (element.Attributes("screenId").Any())
            {
                this.ScreenId = element.Attribute("screenId").Value;
            }

            foreach (var item in element.Elements("keyword"))
            {
                children.Add(new MySubDomain(item));
            }
            this.Children = children;
            this.HasChildren = this.Children.Count() > 0;
        }
    }
    public class Domain
    {
        IEnumerable<MySubDomain> _domains = null;

        public Domain(XDocument root)
        {
            var domains = new List<MySubDomain>();
            foreach (var item in root.Element("root").Elements("keyword"))
            {
                domains.Add(new MySubDomain(item));
            }
            _domains = domains;
        }

        public string Search(IEnumerable<string> phraseList)
        {
            foreach (var item in _domains)
            {
                string lastResult = SearchImpl(phraseList, item);
                if (lastResult != null)
                    return lastResult;
            }
            return null;
        }

        private static string SearchImpl(IEnumerable<string> phraseList, MySubDomain item)
        {
            if (phraseList.Contains(item.Value, StringComparer.InvariantCultureIgnoreCase))
            {
                if (item.HasChildren)
                {

                    foreach (var child in item.Children)
                    {
                        string lastResult = SearchImpl(phraseList.Except(new string[] { item.Value }, StringComparer.InvariantCultureIgnoreCase), child);
                        if (lastResult != null)
                        {
                            return lastResult;
                        }

                    }
                }
                else
                {
                    return item.ScreenId;
                }
            }
            return null;
        }
    }
    public sealed class DomainInputCacheManager
    {
        const string cacheKeyDomain = "fileContentsDomain";
        const string cacheKey = "fileContents";
        string filePath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_data/XMLFile1.xml");



        private static readonly Lazy<DomainInputCacheManager> lazy =
            new Lazy<DomainInputCacheManager>(() => new DomainInputCacheManager());

        public static DomainInputCacheManager Instance { get { return lazy.Value; } }

        private DomainInputCacheManager()
        {

        }

        private ObjectCache _cache = MemoryCache.Default;
        private CacheItemPolicy _policy = new CacheItemPolicy();

        public Domain GetDomainDetails()
        {
            // Check if the data exists in cache
            Domain domainDetails = _cache.Get(cacheKeyDomain) as Domain;

            // If it is null, then go and fetch it
            if (domainDetails == null)
            {
                domainDetails = PopulateCache().Item2;
            }

            return domainDetails;
        }

        public XDocument GetDomainDoc()
        {
            // Check if the data exists in cache
            XDocument domainDoc = _cache.Get(cacheKey) as XDocument;

            // If it is null, then go and fetch it
            if (domainDoc == null)
            {
                domainDoc = PopulateCache().Item1;
            }

            return domainDoc;
        }

        private Tuple<XDocument, Domain> PopulateCache()
        {
            var fileContents = GetDocument(filePath);

            var domainDetails = new Domain(fileContents);
            // Add back into cache with the dependency
            _policy.ChangeMonitors.Add(new HostFileChangeMonitor(new string[] { filePath }));
            // _cache.Add(cacheKey, fileContents, _policy);
            _cache.Add(cacheKeyDomain, domainDetails, _policy);

            return new Tuple<XDocument, Domain>(fileContents, domainDetails);
        }
        private XDocument GetDocument(string filePath)
        {

            return XDocument.Load(filePath);
        }
    }
}